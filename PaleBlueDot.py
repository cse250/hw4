import datetime
import urllib.request
import math
import json

# These imports may be helpful. Feel free to add more if needed


# Location class used to communicate lat/long
class Location:
    # Do not put member variable outside the constructor
    # Variables declared here become static class variables

    # constructor in python. "self" is similar to "this" and must be the first parameter in every method declaration
    def __init__(self, latitude, longitude):
        # self variables declared in the constructor become member variables
        self.latitude = latitude
        self.longitude = longitude if longitude < 180.0 else longitude - 360.0

    def getLatitude(self):
        return self.latitude

    def getLongitude(self):
        return self.longitude

    def __str__(self):
        return "(" + str(self.latitude) + "," + str(self.longitude) + ")"


# The primary class that needs to be implemented. Some skeleton code has been provided, but is not necessary. Feel free
# to structure your code any way you want as long as the required methods function properly.
class PaleBlueDot:
    # Do not put member variable outside the constructor
    # Variables declared here become static class variables

    # Parse files and initialize all data structures. The citiesFile is in the format of worldCities.csv
    def __init__(self, citiesFile):
        self.cityLocations = {}  # initialize a dictionary. You can use any data structure you choose
        self.citySpherical = {}
        self.observatories = {}  # initialize a list. You can use any data structure you choose
        self.parseCities(citiesFile)
        self.getObservatories()
        #self.createBins()

    def createBins(self):
        print("create")
        for i in range(90, -89, -1):
            for k in range(-180, 179, 1):
                key = str(str(i) + "," + str((i-1)) + "," + str(k) + "," + str((k+1)))
                self.citySpherical[key] = []
                #print("key: ", key)


    def assignToBin(self, latitude, longitude):
        #print("latitude: ", latitude)
        #print("longitude: ", longitude)
        #print("assign")
        latlow = math.floor(latitude)
        lathi = math.ceil(latitude)
        if longitude < 0:
            longhi = math.floor(longitude)
            longlow = math.ceil(longitude)
        else:
            longlow = math.floor(longitude)
            longhi = math.ceil(longitude)

        guessKey = str(str(lathi) + "," + str(latlow) + "," + str(longlow) + "," + str(longhi))
        #print("guessKey: ", guessKey)
        #print(guessKey in self.citySpherical)

        if guessKey in self.citySpherical:
            return guessKey



    def parseCities(self, citiesFile):
        self.createBins()
        cities = open(citiesFile, "r")  # open a file in read mode
        cities.readline()  # remove source
        cities.readline()  # remove header
        for line in cities.readlines():
            # Country,City,Region,Latitude,Longitude
            data = line.split(',')
            country = data[0]
            city = data[1]
            region = data[2]
            latitude = float(data[3])
            longitude = float(data[4])

            self.cityLocations[city, region, country] = (Location(latitude, longitude))
            self.citySpherical[self.assignToBin(latitude, longitude)] = [city, region, country, Location(latitude, longitude)]
            #print("citySpherical: ",self.citySpherical)
            #print('33,32,64,65' in self.citySpherical)

        print("citySpherical: ", self.citySpherical)
        cities.close()

    # pull observatory information from the Internet, parse and store.
    def getObservatories(self):
        url = "http://www.minorplanetcenter.net/iau/lists/ObsCodes.html"
        # Open the url in a browser to see the format of the data.
        result = urllib.request.urlopen(url)  # download the data
        observatories = result.read().decode('utf-8')  # read the data as unicode

        i = 0
        for line in (observatories.split("\n")):
            if i < 2 or line == "" or line[13:21].strip(' ') == "" or line == "</pre>" or float(line[13:21].strip(' ')) == 0.0:
                i += 1
            else:
                longitude = float(line[4:13].strip(' '))
                sin = line[21:30].strip(' ')
                cos = line[13:21].strip(' ')
                name = line[30:].strip(' ')
                code = line[0:3].strip(' ')
                latitude = (math.degrees(math.atan(float(sin)/float(cos))))
                self.observatories[code] = (name, latitude, longitude)

    # part 1 - String Parsing

    '''
    Given the name a city, its region, and country, returns its location as a Location object. Returns an empty list
    if the city is unknown, though bad inputs will not be tested for grading so an error can be thrown instead.
    '''

    def getCityLocation(self, city, region, country):
        return self.cityLocations[city, region, country]

    '''
    Given a 3 digit observatory code as a string, return the name of the corresponding observatory.

    Current data can be found here: http://www.minorplanetcenter.net/iau/lists/ObsCodes.html

    Note that this data is not in the most friendly format and care must be taken while parsing. Non-existing codes
    will not be tested.
    '''

    def getObservatoryName(self, observatoryCode):
        #print(self.observatories[observatoryCode][0])
        return self.observatories[observatoryCode][0]








    # part 2 - Math
    '''
    Given a 3 digit observatory code as a string, return the location of the corresponding observatory
    as a Location object with lat /long in degrees. Note that the data is given as
    longitude (in degrees), cos, and sin. Computing atan(sin/cos) will give the latitude in radians.
    Non-existing codes will not be tested.
    '''

    def getObservatoryLocation(self, observatoryCode):
        return Location(self.observatories[observatoryCode][1],self.observatories[observatoryCode][2])
    '''
    Return the great circle distance between two locations on Earth in kilometers. For information on great circle
    distance including sample code in JavaScript see: http://www.movable-type.co.uk/scripts/latlong.html
    '''

    def greatCircleDistance(self, location1, location2):
        rad = 6371000
        phi1 = math.radians(location1.getLatitude())
        phi2 = math.radians(location2.getLatitude())
        long1 = location1.getLongitude()
        long2 = location2.getLongitude()
        delta1 = math.radians(location2.getLatitude() - location1.getLatitude())
        delta2 = math.radians(long2 - long1)

        a = math.sin(delta1/2) * math.sin(delta1/2) + math.cos(phi1) * math.cos(phi2) * math.sin(delta2/2) * \
                                                      math.sin(delta2/2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        d = rad * c
        return d * math.pow(10,-3)

    # part 3

    '''
    Given a location on Earth by lat/long, return the code of the closest observatory in terms of
    great circle distance
    '''

    def getClosestObservatory(self, location):
        dist = 6371001
        observe = ""
        for code in self.observatories:
            d = self.greatCircleDistance(location, self.getObservatoryLocation(code))
            if d<dist:
                dist = d
                observe = code
        return observe

    '''
    Return the code of the observatory that is closest to the ISS. ISS location can be obtained through
    this API: http://api.open-notify.org/
    The result will be in json form which python will parse using "json.loads(jsonData)"
    '''

    def getClosestObservatoryToISS(self):
        url = "http://api.open-notify.org/iss-now.json"
        result = urllib.request.urlopen(url).read().decode('utf-8')
        iss = json.loads(result)
        return self.getClosestObservatory(Location(iss["iss_position"]["latitude"],iss["iss_position"]["longitude"]))

    '''
    Return the next chance to observe the ISS from the given location. Use the same API from the previous
    method, but call "pass time" to retrieve the relevant data. Parsing the JSON will result in a unix
    timestamp that must be converted and returned in a user friendly format via:
    datetime.datetime.fromtimestamp(int("1457985637")).strftime('%Y-%m-%d %H:%M:%S')
    This is the format expected during testing.
    '''

    def nextPassTime(self, location):
        url = "http://api.open-notify.org/iss-pass.json?lat="+location.getLatitude()+"&lon="+location.getLongitude()
        result = urllib.request.urlopen(url).read().decode('utf-8')
        iss = json.loads(result)
        return datetime.datetime.fromtimestamp(int(iss["datetime"])).strftime('%Y-%m-%d %H:%M:%S')

    # part 4

    '''
    Given a location on Earth by lat/long, return the name of the closest city in terms of
    great circle distance. Credit will only be given if the efficiency bound is met. No
    partial credit for correctness. Return the city as a list in the form [city,region,countryCode]
    '''

    def getClosestCity(self, location):
        latlow1 = math.floor(location.getLatitude())
        lathi1 = math.ceil(location.getLatitude())
        longlow1 = math.floor(location.getLongitude())
        longhi1 = math.ceil(location.getLongitude())

        #print("latlow: ", latlow)
        #print("lathi: ", lathi)
        #print("longlow: ", longlow)
        #print("lathi: ", longhi)

        loc = str(str(lathi1) + ',' + str(latlow1) + ',' + str(longlow1) + ',' + str(longhi1))
        #print("latitude: ", location.getLatitude())
        #print("longitude: ", location.getLongitude())
        #print("loc: ", loc)
        r = 1
        bins = []
        if self.citySpherical[loc]:
            bins.append(self.citySpherical[loc])
        loc = loc.split(',')

        while len(bins) < 1:
            for i in range(r):
                #loc = str(loc).split(',')
                #print("loc: ",loc)
                print("run 0")
                lathi = float(loc[0]) + 1
                #print("lathi: ", lathi)
                #print("type: ", type(lathi))
                latlow = float(loc[1]) + 1
                longlow = float(loc[2])
                longhi = float(loc[3])

                loc = str(str(int(lathi)) + ',' + str(int(latlow)) + ',' + str(int(longlow)) + ',' + str(int(longhi)))
                print("loc1: ", loc)
                if self.citySpherical[loc]:
                    bins.append(self.citySpherical[loc])

            for j in range(0, r):
                print("run 1")
                loc = loc.split(',')
                lathi = float(loc[0])
                latlow = float(loc[1])
                longlow = float(loc[2]) + 1
                longhi = float(loc[3]) + 1

                loc = str(str(int(lathi)) + ',' + str(int(latlow)) + ',' + str(int(longlow)) + ',' + str(int(longhi)))
                if self.citySpherical[loc]:
                    bins.append(self.citySpherical[loc])

            for k in range(0, 2*r):
                print("run 2")
                loc = loc.split(',')
                lathi = float(loc[0]) - 1
                latlow = float(loc[1]) - 1
                longlow = float(loc[2])
                longhi = float(loc[3])

                loc = str(str(int(lathi)) + ',' + str(int(latlow)) + ',' + str(int(longlow)) + ',' + str(int(longhi)))
                if self.citySpherical[loc]:
                    bins.append(self.citySpherical[loc])

            for m in range(0, 2*r):
                print("run 3")
                loc = loc.split(',')
                lathi = float(loc[0])
                latlow = float(loc[1])
                longlow = float(loc[2]) - 1
                longhi = float(loc[3]) - 1

                loc = str(str(int(lathi)) + ',' + str(int(latlow)) + ',' + str(int(longlow)) + ',' + str(int(longhi)))
                if self.citySpherical[loc]:
                    bins.append(self.citySpherical[loc])

            for m in range(0, 2*r):
                print("run 4")
                loc = loc.split(',')
                lathi = float(loc[0]) + 1
                latlow = float(loc[1]) + 1
                longlow = float(loc[2])
                longhi = float(loc[3])

                loc = str(str(int(lathi)) + ',' + str(int(latlow)) + ',' + str(int(longlow)) + ',' + str(int(longhi)))
                if self.citySpherical[loc]:
                    bins.append(self.citySpherical[loc])

            for m in range(0, r):
                print("run 5")
                loc = loc.split(',')
                lathi = float(loc[0])
                latlow = float(loc[1])
                longlow = float(loc[2]) + 1
                longhi = float(loc[3]) + 1

                loc = str(str(int(lathi)) + ',' + str(int(latlow)) + ',' + str(int(longlow)) + ',' + str(int(longhi)))
                if self.citySpherical[loc]:
                    bins.append(self.citySpherical[loc])

            loc = str(str(int(lathi1)) + ',' + str(int(latlow1)) + ',' + str(int(longlow1)) + ',' + str(int(longhi1)))
            r = r + 1

        dist = 40075000
        result = []

        for n in bins:
            d = self.greatCircleDistance(location, bins[n][3])
            if d < dist:
                dist = d
                result = [bins[n][0], bins[n][1], bins[n][2]]

        return result

    '''
    Return the closest city to the ISS. Return city as a list in the form [city,region,countryCode]
    '''

    def getClosestCityToISS(self):
        pass
